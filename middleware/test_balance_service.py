from balance_service import BalanceService
from collections import namedtuple
import chain
from chain import Transfer
import json

def test_chain():
    # arrange
    
    empty_block=Block(0, "0", "-1", [], {})
    TestCase = namedtuple('TestCase', 'name given expected')
    Expected = namedtuple('Expected', 'head balances')
    test_cases = [
        TestCase(
            name="empty chain",
            given=[chain.Chain([])],
            expected=Expected(head=None, balances={})
        ),
        TestCase(
            name="one block, no transactions",
            given=[chain.Chain([empty_block])],
            expected=Expected(head=empty_block, balances={})
        ),
        TestCase(
            name="one block with one transaction",
            given=[chain.Chain([Block(0, "0", "-1", [Transfer("bob", "ana", 10)], {})])],
            expected=Expected(
                head=Block(0, "0", "-1", [Transfer("bob", "ana", 10)], {}), 
                balances={
                    "ana": 10,
                    "bob": -10,
            })
        ),
         TestCase(
            name="two blocks",
            given=[chain.Chain([
                Block(0, "0", "-1", [Transfer("bob", "ana", 1)], {
                    "bob": 2,
                    "marty": 3}),
                Block(1, "1", "0", [Transfer("marty", "ana", 2)]),                
                ]),
            ],
            expected=Expected(
                head=Block(1, "1", "0", [Transfer("marty", "ana", 2)]), 
                balances={
                    "ana": 3,
                    "bob": 1,
                    "marty": 1,
            })
        ),
        TestCase(
            name="two branches",
            given=[chain.Chain([
                Block(0, "0", "-1", [Transfer("bob", "ana", 1)], {
                    "bob": 2,
                    "marty": 3}),
                Block(2, "2", "1a", []),
                Block(1, "1b", "0", []),
                Block(1, "1a", "0", [Transfer("marty", "ana", 2)]),
                ]),
            ],
            expected=Expected(
                head=Block(2, "2", "1a", []), 
                balances={
                    "ana": 3,
                    "bob": 1,
                    "marty": 1,
            })
        ),
        TestCase(
            name="second applied with more blocks and another longer branch but less than BLOCK_TOLERANCE",
            given=[
                chain.Chain([
                    Block(0, "0", "-1"),
                    Block(1, "b1:1", "0"),
                    Block(2, "b1:2", "b1:1"),
                    Block(1, "b2:1", "0"),
                ]),
                chain.Chain([
                    Block(0, "0", "-1"),
                    # first branch
                    Block(1, "b1:1", "0"),
                    Block(2, "b1:2", "b1:1"),
                    Block(3, "b1:3", "b1:2"),
                    # second branch
                    Block(1, "b2:1", "0"),
                    Block(2, "b2:2", "b2:1"),
                    Block(3, "b2:3", "b2:2"),
                    Block(4, "b2:4", "b2:3"),
                ]),
            ],
            expected=Expected(
                head=Block(3, "b1:3", "b1:2"),
                balances={})
        ),
        TestCase(
            name="second applied chain with other head > BLOCK_TOLERANCE",
            given=[
                chain.Chain([
                    Block(0, "0", "-1"),
                    Block(1, "b1:1", "0"),
                    Block(2, "b1:2", "b1:1"),
                    Block(1, "b2:1", "0"),
                ]),
                chain.Chain([
                    Block(0, "0", "-1"),
                    # first branch
                    Block(1, "b1:1", "0"),
                    Block(2, "b1:2", "b1:1"),
                    Block(3, "b1:3", "b1:2"),
                    # second branch
                    Block(1, "b2:1", "0"),
                    Block(2, "b2:2", "b2:1"),
                    Block(3, "b2:3", "b2:2"),
                    Block(4, "b2:4", "b2:3"),
                    Block(5, "b2:5", "b2:4"),
                ]),
            ],
            expected=Expected(
                head=Block(5, "b2:5", "b2:4"),
                balances={})
        ),
    ]
    
    for tc in test_cases:
        bs = BalanceService()
        bs.BLOCK_TOLERANCE = 2
        
        # act
        for blockchain in tc.given:  
            bs.apply_chain(blockchain)
    
        # assert
        print tc.name
        assert bs.head == tc.expected.head
        assert bs.balances == tc.expected.balances

def Block(number=0, hash="", prevhash="", transfers=[], balances={}):
    return chain.Block(number, hash, prevhash, Transfers(transfers), balances)

def Transfers(transfers):
    if type(transfers) is dict:
        return [Transfer(**tf) for tf in transfers]
    return transfers

