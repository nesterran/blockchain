import hashlib
from balance_calculator import Calculator
import chain

class BalanceService(object):
    # This constant determine how many more blocks
    # other branch must have to be consider the main branch
    BLOCK_TOLERANCE = 3

    def __init__(self):
        self.balances = dict()
        self.median = 0
        self.branch = []
        self.balance_calc = Calculator()
        self.subscribers = dict()
    
    def apply_chain(self, blockchain):
        """
        Apply the given chain and calculate the final balance.
        This method is idempotent, so if the same chain is applied
        multile times it garanties the same balance

        @type blockchain: chain.Chain
        @param blockchain: The given chain        
        """
        if len(blockchain.blocks) == 0:
            return
        root, dic = blockchain.to_tree_dic()
        max_leaf = chain.max_leaf(root)
        head = max_leaf.block
        # gets the branch of blocks from the max leaf
        new_branch = chain.to_branch(max_leaf)
        
        # If empty balance process the whole new branch
        if not self.head:
            self.apply_branch(new_branch)
            return
        elif self.head.number >= head.number:
            # chain has no more blocks
            return

        # new chain must have more blocks
        assert len(new_branch) > len(self.branch)

        # if new branch is an update from current head's branch
        if chain.block_in_branch(self.head, new_branch):
            self.apply_branch(new_branch)
            return

        # if there is an update to current head's branch
        head_node = dic[self.head.hash]
        leaf = chain.max_leaf(head_node) 
        updated_branch = chain.to_branch(leaf)

        # if new branch should be applied
        skip_new_branch = len(updated_branch) + self.BLOCK_TOLERANCE > len(new_branch)
        if not skip_new_branch:
            self.apply_branch(new_branch)
            return
        
        # current branch should be updated
        if leaf.block.hash != self.head.hash:            
            self.apply_branch(updated_branch)

    @property
    def head(self):
        if len(self.branch) > 0:
            return self.branch[-1]
        return None

    def apply_branch(self, branch):
        (new_balance, median) = self.balance_calc.calculate(branch)
        
        # remove unchanged balance entries
        for key, value in self.balances.iteritems():
            if new_balance[key] == value:
                del new_balance[key]

        self.balances.update(new_balance)
        self.branch = branch
        self.median = median
        if len(new_balance) > 0:
            self.__notify_changes(new_balance)
        return

    def __notify_changes(self, balance_changes):
        callback_args = dict()
        for key, _ in balance_changes.iteritems():
            if key in self.subscribers:
                callback = self.subscribers[key]
                if callback not in callback_args:
                    callback_args[callback] = []    
                callback_args[callback].append(key)
        
        for callback, keys in callback_args.iteritems():
            callback(keys)

    def subscribe(self, accounts, callback):
        for acc in accounts:
            if acc not in self.subscribers:
                self.subscribers[acc] = []    
            self.subscribers[acc].append(callback)

def hexhash(x):
    return '0x' + hashlib.sha224(str(x)).hexdigest()[:6]