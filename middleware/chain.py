from recordtype import recordtype
from collections import namedtuple

Block = namedtuple('Block', 'number hash prevhash transfers balances')
Transfer = namedtuple('Transfer', 'sender receiver amount')
Node = recordtype('Node', 'block parent children')

class Chain(object):

    def __init__(self, blocks=[]):
        self.blocks = blocks

    def to_tree_dic(self):
        """
            This method creates a tree representation of the blocks and returns
            the root node together with a dictionary of the block hashes->nodes
        """
        dic = {block.hash: Node(block, None, []) for block in self.blocks}
        root = None
        for _, node in dic.iteritems():
            if node.block.prevhash not in dic:
                root = node
                continue

            parent = dic[node.block.prevhash]
            parent.children.append(node)            
            node.parent = parent            
            
       
        return root, dic
    
    def max_block(self):
        num = -1
        hblock = None
        for block in self.blocks:        
            if block.number > num:                
                hblock = block
                num = block.number
        
        return hblock       

def block_in_branch(block, branch):
    for b in branch:
        if b.hash == block.hash:
            return True
    return False

def to_branch(node):
    branch = [node.block]
    while node.parent:
        node = node.parent
        branch.append(node.block)
    
    branch.reverse()
    return branch


def max_leaf(parent_node):
    if not parent_node:
        return None

    max_node = parent_node
    nodes = [parent_node]
    while len(nodes) > 0:
        node = nodes.pop()
        if node.block.number > max_node.block.number:
            max_node = node
        
        nodes += node.children

    return max_node
