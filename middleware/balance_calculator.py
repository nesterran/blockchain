
class Calculator(object):
    # It determines how many balances are cached from a branch
    BALANCE_CACHE_LENGTH = 6
    
    def __init__(self):
        self.balances_cache = dict()
    
    def calculate(self, branch):
        # get a copy of the affected balances
        # this copy prevents showing partial data
        # to the client
        if len(branch) == 0:
            return {}, 0

        # get initial balance and cache index
        cache_idx = -1
        for idx in range(len(branch) -1, -1, -1):
            block = branch[idx]
            if block.hash in self.balances_cache:
                cache_idx = idx
                break
        
        init_balance = {}
        if cache_idx >= 0:
            init_balance = self.balances_cache[branch[idx].hash]        
        # if there is a initial balance take it!
        elif len(branch) >= 0 and branch[0].balances:
            init_balance = branch[0].balances       
            
        balances = init_balance.copy()
        print "init:" + str(balances)                
        # apply uncached blocks
        for i in range(cache_idx + 1, len(branch)):
            block = branch[i]
            for tf in block.transfers:
                # REMARK: This assumes that new accounts
                # can be automaticaly added with new transfer
                if tf.sender not in balances:
                    balances[tf.sender] = 0    
                if tf.receiver not in balances:
                    balances[tf.receiver] = 0    

                balances[tf.sender] -= tf.amount
                balances[tf.receiver] += tf.amount
                # TODO: Update the median incrementaly       
                
            self.balances_cache[block.hash] = balances

        # Keep cache length on the given branch
        new_cache = dict()
        for i, block in enumerate(reversed(branch)):
            if i > self.BALANCE_CACHE_LENGTH:
                break
            new_cache[block.hash] = self.balances_cache[block.hash]
        
        self.balances_cache = new_cache
        return (self.balances_cache[branch[-1].hash].copy(), 0)