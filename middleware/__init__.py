import os
import sys

# adds current directory to the system path
file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)