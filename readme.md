# Description

The `middleware` package contains the middleware implementation code.
The main class is the `balance_service.BalanceService`, and the
main method to process a blockchain is `apply_chain`, the final balance is
stored in the  In the `balance_service.BalanceService.balances` attribute.
For coding examples please take a look to the unit tests in file `test_balance_service.py`

In order to subscribe to accounts' balance updates a `subscribe` method is exposed on the `BalanceService`, 
it expects a list of `accounts` and a callback function to be called when any of the accounts got updated.
The callback function is called with the list of updated accounts so that the client can perform an informed
decision about what to update.

# Algorithm

To process or reply a *blockchain* the algorithm search for the longest branch and apply its block's transfer to obtain the final balance. With a new *blockchain* version the algorithm performs the following actions:

- If the longest branch is still the algorithm's current working branch then it applies the new blocks' transfers on its current balances.
- If the longest branch is not the algorithm's current working branch *and* it surpasses in blocks/nodes by more than the tolerance threshold (`BLOCK_TOLERANCE`) then this branch becomes the algorithm's woking branch and balances are calculated based on its blocks' `transfers`. Note that the algorithm always keeps a cache of some previous blocks (defined by `BALANCE_CACHE_LENGTH`) so in most cases it is not needed to apply all blocks' transfers from the new branch. The global setting `BALANCE_CACHE_LENGTH` is set to six by default since it is very unlikely that a new branch will diverge for more than six blocks. In the unlikely case that it happens then the whole branch's transfers are used to calculate the new balances.
- If the longest branch does not exceeds the tolerance factor, the *main* branch is updated as if it were the longest one.

Note that the algorithm calculates the *diff* on the balance for every new blockchain update and will notify the subscribers/clients *IIF* their acounts' balances of interest changed.


# TODOs

- The `BalanceService` misses a method to query for a subset of account's balances. The most simple implementation would be a filter over the `BalanceService.balances`.
- The median is still not implemented, but the idea is to keep a sorted list of accounts to easily calculate the `meidan`. If new updates comes then the sorted list is updated accordignly and also its median.
- Add unit tests to the notification of subscribers

# Considerations
- The given blockchain is not expected to be given in a specific order.
- The balances' cache keeps by default the six previous balances, since reverting more than six blocks is very unlikely, this setting can be changed by global the config variable `BALANCE_CACHE_LENGTH`.
- The balances are calculated based on the chain's *main* branch. The *main* branch is the longest branch in the given blockchain. With new blockchain updates, if there is a competing longer branch, it becomes the *main* branch *IIF* it exceeds by more blocks than the `BLOCK_TOLERANCE` factor.

# Tasks Anwers
 - Describe an efficient solution for above with the added constraint, that the complete data structure doesn’t fit into memory: If by *complete data structure* means the Whole chain, then it would be sufficient to keep a cache of some previos blocks' balances from the main branch (six blocks as recommended) and block headers (hash, previous hash and number). Depending on the memory constrains this cache can be keep in-memory, or a in-memory distributed database, or a durable distributed storage. But in general it is a trade-off between memory, network traffic, storage and fault handling, and the storage is modeled based on the access pattern.
 - Describe what needs to be considered in the client app: The most important part form my opinino is how the client gets the balances updates.  Depending on the frequency a pull or push model can be used, for high frequent updates a push model with a open connection is more efficient since it prevents the overhead of negotiating a connection on every update; for low frequent updates an server endpoint can be requested for updates recurrenly.
 - Describe which edge cases you can think about: A new branch diverges for more than six blocks
 - Describe a system architecture and communication pattern for updating the client app: Already anwered.
 - Which fields of computer science address the described problem?: Data Structures(trees, dictionaries), traversing a tree, dinamic programming, Computational Complexity, Caching 

# How to install it

- Clone the repository
- CD into the repository
- run `pip install -r requirements`

# Run tests

please run `pytest middleware/test*.` on the root repository directory